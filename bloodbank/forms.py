from django.forms import ModelForm
from .models import Donor, ContactUs


class DonorForm(ModelForm):
    class Meta:
        model = Donor
        fields = ['name', 'address', 'email', 'DOB', 'bloodgroup', 'Date_of_last_donation', 'gender', 'state', 'weight',
                  'not_suffer', 'eligibility', 'profile_pic']


class ContactForm(ModelForm):
    class Meta:
        model = ContactUs
        fields = ['email', 'subject', 'message']
