from django.urls import path
from .views import index, register, contactus, aboutus, campaign, what_is_blood, happy_giving, become_blood_donor, \
    gallery, donorinfo

app_name = 'bloodbank'

urlpatterns = [
    path('', index, name='index'),
    path('register', register, name='register'),
    path('aboutus', aboutus, name='aboutus'),
    path('campaign', campaign, name='campaign'),
    path('contactus', contactus, name='contactus'),
    path('what_is_blood', what_is_blood, name='what_is_blood'),
    path('happy_giving', happy_giving, name='happy_giving'),
    path('what_is_blood', what_is_blood, name='what_is_blood'),
    path('become_blood_donor', become_blood_donor, name='become_blood_donor'),
    path('gallery', gallery, name='gallery'),
    path('donorinfo', donorinfo, name='donorinfo'),

]
