from django.shortcuts import render, redirect
from .models import Donor, Campaign, ContactUs, Gallery
from .forms import DonorForm, ContactForm
from django.contrib import messages
from django.db.models import Q
from django.http import HttpResponseRedirect


# from django.core.paginator import Paginator


# Create your views here.

def index(request):
    return render(request, 'index.html')


def aboutus(request):
    return render(request, 'aboutus.html')


def campaign(request):
    campaign = Campaign.objects.all()
    context = {
        'campaign': campaign
    }
    return render(request, 'campaign.html', context)


def what_is_blood(request):
    return render(request, 'what-_is_blood.html')


def become_blood_donor(request):
    return render(request, 'become_blood_donor.html')


def gallery(request):
    gallery = Gallery.objects.all().order_by('pub_date')
    context = {
        'gallery': gallery
    }
    return render(request, 'gallery.html', context)


def happy_giving(request):
    return render(request, 'happy_giving.html')


def contactus(request):
    contactus = ContactUs.objects.all()
    form = ContactForm()

    if request.method == 'POST':
        print(request.POST)
        form = ContactForm(request.POST)
        if form.is_valid():
            form.save()
            messages.success(request, 'Message send successfully.')

        else:
            print('error')
            messages.error(request, 'Messages are not sent.')
    context = {
        'contactus': contactus,

        'form': form
    }

    return render(request, 'contactus.html', context)


def register(request):
    donor = Donor.objects.all()
    form = DonorForm()

    if request.method == 'POST':
        print(request.POST)
        form = DonorForm(request.POST, request.FILES)

        if form.is_valid():
            form.save()
            messages.success(request, 'Message send successfully.')
        else:
            print('error')
            messages.error(request, 'Messages are not sent.')

    context = {
        'donor': donor,
        'form': form
    }
    return render(request, 'register.html', context)


def donorinfo(request):
    if request.method == 'POST':
        blood_group = request.POST['bloodgroup']
        addresses = request.POST['address']
        if blood_group:
            if addresses:
                match = Donor.objects.filter(Q(bloodgroup__icontains=blood_group) &
                                             Q(address__icontains=addresses)
                                             )
                # paginator = Paginator(match, 3)
                # page = request.GET.get('page')
                # match = paginator.get_page(page)
                if match:
                    context = {
                        'match': match
                    }
                    return render(request, 'donorinfo.html', context)
                else:
                    messages.error(request, 'No result found!!')
        else:
            return HttpResponseRedirect('/donorinfo/')

    return render(request, 'donorinfo.html')
