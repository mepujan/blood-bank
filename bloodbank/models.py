from django.db import models
from froala_editor.fields import FroalaField


# Create your models here.
class Donor(models.Model):
    name = models.CharField(max_length=30)
    address = models.CharField(max_length=30)
    email = models.EmailField()
    DOB = models.DateField()
    bloodgroup = models.CharField(max_length=10)
    Date_of_last_donation = models.DateField()
    gender = models.CharField(max_length=6)
    state = models.CharField(max_length=10)
    weight = models.FloatField()
    not_suffer = models.CharField(max_length=100)
    eligibility = models.CharField(max_length=10)
    profile_pic = models.ImageField(upload_to='profile_pic')

    def __str__(self):
        return self.name


class Campaign(models.Model):
    campaign = models.ImageField()
    descriptions = FroalaField()


class ContactUs(models.Model):
    email = models.EmailField()
    subject = models.CharField(max_length=50)
    message = models.TextField()

    class Meta:
        verbose_name_plural = "Contact Us"


class Gallery(models.Model):
    image = models.ImageField()
    pub_date = models.DateField()

    class Meta:
        verbose_name_plural = "Galleries"




