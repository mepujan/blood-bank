from django.contrib import admin
from .models import Donor,Campaign,Gallery,ContactUs

# Register your models here.
admin.site.register(Donor)
admin.site.register(Campaign)
admin.site.register(Gallery)
admin.site.register(ContactUs)

